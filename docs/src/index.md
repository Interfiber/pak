# Pak Docs
MacOS package installer builder

## Downloads
<details>
<summary>Download Links</summary>
<br>
<a href="https://gitlab.com/Interfiber/pak/uploads/db44f3485772f66d3a4e9fa37fb2ded8/installer.pkg">Download v1.0.1</a>

<a href="https://gitlab.com/Interfiber/pak/uploads/9cdd38569a5057cb00f1e516d186b13c/installer.pkg">Download v0.2.1</a>

<a href="https://gitlab.com/Interfiber/pak/uploads/5f506877c4500e1616a57a12915dfe07/installer.pkg">Download v0.2.0</a>

</details>


## What does pak do?
Pak builds MacOS package installers from a project file. Since apple removed PackageBuilder from xcode it became harder to build packages
for software on MacOS, this is what pak aims to fix.
